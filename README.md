# GitLab Runner Demo

An attempt to create a demonstration of GitLab Runner for the [Technology 
Community of Practice](https://intranet.uis.cam.ac.uk/how/cop), so people 
can get an undertanding of how to set it up and use it.

It requires the user to be able to install software on their own desktop
to be able to run a VM.

# Instructions

For instructions, see [the wiki](../-/wikis/home).

# Help

You could:
* raise an issue in this repository
* If you are in UIS, post in the tech-cop slack channel.
